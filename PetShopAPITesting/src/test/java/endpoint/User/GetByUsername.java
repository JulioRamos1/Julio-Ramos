package endpoint.User;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.restassured.RestAssured;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;


public class GetByUsername {
    protected final String apiBasePath = "/user/";
    protected final String username1 = "user903";
    
    protected final String UserNotFound = "UserNotFound";
    protected final String InvalidUserName = null;
	
    @BeforeMethod
    public static void setup() {
        RestAssured.baseURI = "http://localhost:8080/api/v3";
    }
	
	@Test
	public void test_GettingUserByUSerName_UserExists() {
	   get(apiBasePath+username1).then().statusCode(200).assertThat().log().all()
	      .body("username", equalTo(username1));
	}
	
	@Test
	public void test_GettingUserByUSerName_UserNotFound() {
	   get(apiBasePath+UserNotFound).then().statusCode(404).assertThat().log().all();
	}
	
	@Test
	public void test_GettingUserByUSerName_InvalidUserName() {
	   get(apiBasePath+InvalidUserName).then().statusCode(400).assertThat().log().all();
	}
	

	

}
