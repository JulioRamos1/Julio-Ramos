package endpoint.User;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import org.testng.Assert;
import static io.restassured.RestAssured.*;

public class GetUserLogout {
	
	@BeforeMethod
    public static void setup() {
        RestAssured.baseURI = "http://localhost:8080/api/v3/user/logout";
    }

	
	@Test
	public void test_UserLogout_userShouldBeLoggedOut_ValidateHeader() {

		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.get();
		
		String responseBody = response.getBody().asString();
		System.out.println("Response Body is =>  " + responseBody);
		String responseHeader = response.getHeaders().toString();
		System.out.println("Response Header is =>  " + responseHeader);
		
		Assert.assertEquals(response.getHeader("access-control-allow-headers"), "Content-Type, api_key, Authorization");
		Assert.assertEquals(response.getHeader("access-control-allow-methods"), "GET, POST, DELETE, PUT");
		Assert.assertEquals(response.getHeader("access-control-allow-origin"), "*");
		Assert.assertEquals(response.getHeader("access-control-expose-headers"), "Content-Disposition");
		Assert.assertEquals(response.getHeader("content-length"), "15");
		Assert.assertEquals(response.getHeader("content-type"), "application/json");
	
		
		Assert.assertEquals(response.getStatusCode(), 200);
		Assert.assertEquals(response.contentType(), ContentType.JSON.toString());
		Assert.assertTrue(responseBody.contains("User logged out"));
		
	}
	
	@Test
	public void test_USerLogout_userSHouldBeLoggedOut2() {
	   get().then().statusCode(200).assertThat().and().log().all();
	}	
	
}
