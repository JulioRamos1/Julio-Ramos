package endpoint.User;

import org.testng.annotations.*;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;


public class GetUserLogin {

	@BeforeMethod
    public void setup() {
        RestAssured.baseURI = "http://localhost:8080/api/v3/user";
    }
	
    @DataProvider(name = "userData")
    public String[][] UserData() {
    	return new String[][] {
    		{"test", "abc123", "200", "Logged in user session"},
    		{"test", "abc123", "200", "Logged in user session"},
    		{"test", "InvalidPassword", "400", "Invalid username/password supplied"},
    		{"invalidUSer", "abc123", "400", "Invalid username/password supplied"}
    	};
    }
	
	
	@Test (dataProvider="userData")
	public void test_APIWithBasicAuthentication(String username, String password, String responseCode, String expectedMsg) {

		int respCode = 0;
		try{
			respCode = Integer.parseInt(responseCode);
        }
        catch (NumberFormatException ex){
            ex.printStackTrace();
        }
		
		RequestSpecification request = RestAssured.given().urlEncodingEnabled(true);
		Response response = request
					.queryParam("username", username)
					.queryParam("password", password)
					.get("/login");

		
		System.out.println(response.getBody().asString());
		
		Assert.assertEquals(response.getStatusCode(), respCode);
		Assert.assertEquals(response.contentType(), ContentType.JSON.toString());
		Assert.assertTrue(response.getBody().asString().contains("Logged in user session"));
	}
}
