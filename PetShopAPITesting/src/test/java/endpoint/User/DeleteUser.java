package endpoint.User;

import static io.restassured.RestAssured.given;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.testng.annotations.*;


public class DeleteUser {
	
	@BeforeMethod
    public void setup() {
        RestAssured.baseURI = "http://localhost:8080/api/v3";
    }
	
	
	@DataProvider(name = "deleteUserData")
    public String[][] deleteUserData() {
    	return new String[][] {
    		{"user900", "200"},
    		{"user901", "200"},
    		{"user999", "404"}
    	};
    }
	
	@Test(dataProvider="deleteUserData")
	public void test_DeleteUsers(String username, String responseCode) {
        
		int respCode = 0;
		try{
			respCode = Integer.parseInt(responseCode);
        }
        catch (NumberFormatException ex){
            ex.printStackTrace();
        }
        
        given().urlEncodingEnabled(true)
            .header("Accept", ContentType.JSON.getAcceptHeader())
            .delete("/user/"+username)
            .then()
            	.log().all()
            	.statusCode(respCode);
	}
	
	@Test
	public void test_DeleteUser_VerifyResponse() {
		int respCode = 200;
		String username = "User902";
        
        given().urlEncodingEnabled(true)
			.param("username", username)
            .header("Accept", ContentType.JSON.getAcceptHeader())
            .delete("/user/" + username)
            .then()
            	.statusCode(respCode)
            .and()
            	.contentType(ContentType.JSON.toString());
	}

}
