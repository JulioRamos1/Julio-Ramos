package endpoint.User;

import static io.restassured.RestAssured.given;
import org.testng.annotations.*;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;


public class PostUser {

	@BeforeMethod
    public void setup() {
        RestAssured.baseURI = "http://localhost:8080/api/v3";
    }
	
    @DataProvider(name = "userData")
    public String[][] createUserData() {
    	return new String[][] {
    		{"900", "user900", "Name900", "LastName900", "900@gmail.com", "90078432", "900995350238", "1", "200"},
    		{"901", "user901", "Name901", "LastName901", "901@gmail.com", "90178432", "901995350238", "1", "200"},
    		{"902", "user902", "Name902", "LastName902", "902@gmail.com", "90278432", "902995350238", "1", "200"},
    		{"902", "user902", "Name902", "LastName902", "902@gmail.com", "90278432", "902995350238", "1", "400"},
    		{"903", "user903", "Name903", "LastName902", "902@gmail.com", "90278432", "902995350238", "1", "200"},
    		{"904", "user904", "Name904", "LastName902", "902@gmail.com", "90278432", "902995350238", "1", "200"},
    		{"905", "user905", "Name905", "LastName902", "902@gmail.com", "90278432", "902995350238", "1", "200"},
    		{"906", "user906", "Name906", "LastName902", "902@gmail.com", "90278432", "902995350238", "1", "200"}
    		
    	};
    }
    
   
	@Test(dataProvider="userData")
	public void test_CreatingUser_UserShouldBeCreated(String id, String username, String firstName, String lastName, String email, String password, String phone, String userStatus, String responseCode) {
        
		int respCode = 0;
		try{
			respCode = Integer.parseInt(responseCode);
        }
        catch (NumberFormatException ex){
            ex.printStackTrace();
        }
        
        given().urlEncodingEnabled(true)
            .param("id", id)
			.param("username", username)
			.param("firstName", firstName)
			.param("lastName", lastName)
			.param("email", email)
			.param("password", password)
			.param("phone", phone)
			.param("userStatus", userStatus)
            .header("Accept", ContentType.JSON.getAcceptHeader())
            .post("/user")
            .then()
            	.statusCode(respCode)
            .and()
            	.contentType(ContentType.JSON.toString());
    }

}
