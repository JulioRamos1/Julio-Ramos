package getRequest;

import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;

public class GetData {
	String API = "http://localhost:8080/api/v3/openapi.json";
	
	@Test
	public void testResponseCode() {
		int code = get(API).getStatusCode();
		System.out.println("Code: " + code);
		Assert.assertEquals(code, 200);
	}
	
	@Test
	public void testBody() {
		Long time=get(API).getTime();
		System.out.println("Time: " + time);
	}

}
