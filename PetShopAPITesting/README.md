# Test Strategy for the Swagger Pet Shop Store

## How to Run the Tests
  This test automation was built with TestNG and Rest Assured, using Eclipse.
  
  After opening the project in Eclipse, locate the  testng.xml file in the Package Explorer panel. Right click on it and then select RunAs - TestNG Suite:  
  ![image](https://user-images.githubusercontent.com/13575588/114594880-0fd77680-9c8e-11eb-9070-0e9303c1670e.png)
  
  Tests will be executed and the results can be seen in the Console and also in the TestNG Test Output console, as shown below:
  ![image](https://user-images.githubusercontent.com/13575588/114595318-8bd1be80-9c8e-11eb-9c37-a21c9867845a.png)
  

## Scope And Overview

  The objective of this document is to create a test strategy for testing the Swagger Pet Shop Store.
  
## API testing requirements

    The purpose of the API is to provide a simple PetShop interface for managing a small Pet Shop store. The target consumers of the API are Pet Shop owners and employees,
    and other developers.

    The API provides 3 endpoints:
      * pet
      * store
      * users

    The first endpoint, PETS, provides an interface for adding, deleting and updating a pet, to search a pet using different variables, to upload a pet photo.

    The store endpoint allows the user to place or to delete an order for a pet, to get an order and to request an inventory

    The user endpoint let the user to add, delete, update an user, to request user data and to login and logout

    As we have a very limited time, our automation process will focus on the most important features of the USer endpoint:
    * USER: create user, login, logout, get user, delete user

    Features not to be tested: 

## Test Approach

  Functional API tests.
    
    Functional tests are the focus of this test plan. The following set will be considered during development of the test cases:
      * Verify correct HTTP status code
      * Verify response body
      * Verify response headers
      * Verify basic performance: time to compete an operation

  Test Scenarios for funciotnal tests:
      * Positive tests
      * Negative tests

  Test Flows for functional tests:
      * Testing requests in isolation
      * Multi-step workflow with several requests

  * API performance tests

    Out of the scope due to lack of time
    
  * API security tests
    
    Out of the scope due to lack of time
  
  * Integration Tests
    Out of scope as there's no other apps using the API

## Test entry and exit criterias
    Test entry criteria: API is deployed
    Test exit criteria: No high priority bug is open, 95% of medium priority bugs are fixed

## Testing Tools
    Java JDK 16, Eclipse
    The API can be deployed using Maven or Dockers
    Libraries: rest-assured, testNG

## Testing Results
    There are 6 test cases that are failing. Let's analyse them together:
    
    # PostUser - test_CreatingUser_UserShouldBeCreated("902", "user902", "Name902", "LastName902", "902@gmail.com", "90278432", "902995350238", "1", "400")
    
    This test case tries to create a user with an id that is already in use by another user, so the test case expects that some error response would be received, but in fact, the user is being rewritten.
    
    # DeleteUser - test_DeleteUsers("user999", "404")
    
    Test expects that trying to delete a non existant user would return 404, but it's returning 200 (success)
    
    # DeleteUser - test_DeleteUser_VerifyResponse
    
    The test expects a JSON response after a successful  execution, but the response is empty
    
    # GetByUsername - test_GettingUserByUSerName_InvalidUserName
    
    The test expects that an invalid user would return 400, but it's returning 404 (user not found)
    
    # GetUserLogin - test_APIWithBasicAuthentication ("test", "InvalidPassword", "400", "Invalid username/password supplied")
    
    # GetUserLogin - test_APIWithBasicAuthentication {"invalidUSer", "abc123", "400", "Invalid username/password supplied"}
    
    In both cases, test expects that a invalid user or login would return an error response code and\or an warning message. ut it's returning 200 and a session ID.
    

